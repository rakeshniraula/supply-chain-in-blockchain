from flask import Flask

app = Flask(__name__)

from app import views

import flask_login


login_manager = flask_login.LoginManager()

login_manager.init_app(app)

