
from flask import redirect, render_template, request

import json

import flask_login
from flask_login import login_manager

from app import app

import blockchain_db as db



@app.route('/')
def index():
    return render_template("index.html")


@app.route('/register', methods=["GET", "POST"])
def register():
    if request.method == "POST":

        data = {"name": request.form.get('name'),
                "party_type": str(request.form.get('select')),
                "password": request.form.get('password')}

        if str(request.form.get('select')) == "producer":
            db.add_record(data, "producer")
        else :
            db.add_record(data, "distributor")
            

        

    return render_template("index.html")
