import os
import json
import errno

import blockchain

def create_dir(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)


def add_record(data, fname):
    create_dir('db')
    fname = 'db/' + fname + '.json'
    a = []
    if not os.path.isfile(fname) or os.stat(fname).st_size == 0:
        a.append(data)
        with open(fname, 'w', encoding='utf-8') as f:
            json.dump(a, f, ensure_ascii=False, indent=4)
    else:
        with open(fname, mode='r') as feedsjson:
            feeds = json.load(feedsjson)

        feeds.append(data)
        with open(fname, 'w', encoding='utf-8') as f:
            json.dump(feeds, f, ensure_ascii=False, indent=4)
# def add_record(data, fname):
#     create_dir('db')
#     fname = 'db/' + fname + '.json'
#     a = []
#     if not os.path.isfile(fname):
#         a.append(data)
#         with open(fname, mode='w') as f:
#             f.write(json.dumps(a, indent=4))
#     else:
#         with open(fname, mode='r') as feedsjson:
#             feeds = json.load(feedsjson)

#         feeds.append(data)
#         with open(fname, mode='w') as f:
#             f.write(json.dumps(feeds, indent=4))
    
    def add(data):
        with open('data.json', 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)


    def fetch_json(fname):
        fname = 'db/' + fname + '.json'
        if not os.path.isfile(fname) or os.stat(fname).st_size == 0:
            with open(fname, 'r') as read_file:
                data = json.load(read_file)

            return data

        else: return False
